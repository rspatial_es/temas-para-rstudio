# Temas para Rstudio

En este repositorio se muestran dos temas para el grupo de Rspatial_es.

## R Espacial ES oscuro

![](previews/dark.png)

## R Espacial ES claro

![](previews/light.png)

# Instalación

Se pueden instalar usando `rstudioapi` o desde la interfaz de opciones globales.

```r
rstudioapi::addTheme("ruta/fichero_tema.rstheme")
```

o

![](img/global_options.png)
